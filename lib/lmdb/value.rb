module LMDB
  ##
  # LMDB value.
  #
  # This is a generic structure used to pass keys and values in and out of
  # the database.
  #
  # @see http://symas.com/mdb/doc/group__mdb.html#structMDB__val
  class Value < FFI::Struct
    layout :size, :size_t,
           :data, :pointer

    ##
    # Initializes a new value.
    #
    # @param [FFI::Pointer] data
    # @param [Integer] size
    def initialize(data = nil, size = nil)
      self[:size] = size if size
      self[:data] = data if data
    end

    ##
    # The size of the data.
    #
    # @return [Integer]
    attr_reader :size
    def size
      self[:size]
    end

    ##
    # The pointer to the data.
    #
    # @return [FFI::Pointer]
    attr_reader :data
    def data
      self[:data]
    end
  end # Value
end # LMDB
