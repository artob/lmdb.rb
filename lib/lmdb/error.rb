module LMDB
  ##
  # LMDB error.
  class Error < StandardError
    include Library

    ##
    # Raises an error.
    #
    # @param [Integer] rc
    # @param [String] func
    def self.raise(rc, func)
      Kernel.raise Error.new(rc, func)
    end

    ##
    # Initializes an error exception.
    #
    # @param [Integer] rc
    # @param [String] func
    def initialize(rc, func)
      super("%s: %s" % [func, Library.mdb_strerror(rc)])
    end
  end # Error
end # LMDB
