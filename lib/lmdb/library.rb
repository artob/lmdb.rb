require 'ffi' # @see https://rubygems.org/gems/ffi

module LMDB
  ##
  # Foreign function interface to the LMDB shared library.
  module Library
    extend FFI::Library

    ##
    # Returns the LMDB version string.
    #
    # This is a string in the format of:
    # `"LMDB 0.9.14: (September 20, 2014)"`.
    #
    # @return [String]
    def self.version
      self.mdb_version(nil, nil, nil)
    end

    ffi_lib case RUBY_PLATFORM
      when /-darwin/ then %w(lmdb /opt/local/lib/liblmdb.dylib /sw/lib/liblmdb.dylib)
      when /-linux/  then %w(liblmdb.so.0 lmdb)
      else %w(lmdb)
    end

    # Error codes
    MDB_SUCCESS          = 0
    MDB_KEYEXIST         = (-30799)
    MDB_NOTFOUND         = (-30798)
    MDB_PAGE_NOTFOUND    = (-30797)
    MDB_CORRUPTED        = (-30796)
    MDB_PANIC            = (-30795)
    MDB_VERSION_MISMATCH = (-30794)
    MDB_INVALID          = (-30793)
    MDB_MAP_FULL         = (-30792)
    MDB_DBS_FULL         = (-30791)
    MDB_READERS_FULL     = (-30790)
    MDB_TLS_FULL         = (-30789)
    MDB_TXN_FULL         = (-30788)
    MDB_CURSOR_FULL      = (-30787)
    MDB_PAGE_FULL        = (-30786)
    MDB_MAP_RESIZED      = (-30785)
    MDB_INCOMPATIBLE     = (-30784)
    MDB_BAD_RSLOT        = (-30783)
    MDB_BAD_TXN          = (-30782)
    MDB_BAD_VALSIZE      = (-30781)
    MDB_BAD_DBI          = (-30780)
    MDB_LAST_ERRCODE     = MDB_BAD_DBI

    # Type definitions
    typedef :pointer, :MDB_cursor
    enum :MDB_cursor_op, %i(
      MDB_FIRST MDB_FIRST_DUP
      MDB_GET_BOTH MDB_GET_BOTH_RANGE MDB_GET_CURRENT MDB_GET_MULTIPLE
      MDB_LAST MDB_LAST_DUP
      MDB_NEXT MDB_NEXT_DUP MDB_NEXT_MULTIPLE MDB_NEXT_NODUP
      MDB_PREV MDB_PREV_DUP MDB_PREV_NODUP
      MDB_SET MDB_SET_KEY MDB_SET_RANGE
    )
    typedef :uint, :MDB_dbi
    typedef :pointer, :MDB_env
    typedef :pointer, :MDB_envinfo
    typedef :pointer, :MDB_stat
    typedef :pointer, :MDB_txn
    typedef :pointer, :MDB_val

    # Callback definitions
    callback :MDB_assert_func, [:MDB_env, :string], :void
    callback :MDB_cmp_func, [:MDB_val, :MDB_val], :int
    callback :MDB_msg_func, [:string, :pointer], :int
    callback :MDB_rel_func, [:MDB_val, :pointer, :pointer, :pointer], :void

    # Functions for library metadata
    attach_function :mdb_version, [:pointer, :pointer, :pointer], :string
    attach_function :mdb_strerror, [:int], :string

    # Functions on MDB_env* handles
    attach_function :mdb_env_create, [:pointer], :int
    attach_function :mdb_env_open, [:MDB_env, :string, :uint, :int], :int
    attach_function :mdb_env_copy, [:MDB_env, :string], :int
    attach_function :mdb_env_copyfd, [:MDB_env, :int], :int
    #attach_function :mdb_env_copy2, [:MDB_env, :string, :uint], :int # @since 0.9.14
    #attach_function :mdb_env_copyfd2, [:MDB_env, :int, :uint], :int # @since 0.9.14
    attach_function :mdb_env_stat, [:MDB_env, :MDB_stat], :int
    attach_function :mdb_env_info, [:MDB_env, :MDB_envinfo], :int
    attach_function :mdb_env_sync, [:MDB_env, :int], :int
    attach_function :mdb_env_close, [:MDB_env], :void
    attach_function :mdb_env_set_flags, [:MDB_env, :uint, :int], :int
    attach_function :mdb_env_get_flags, [:MDB_env, :pointer], :int
    attach_function :mdb_env_get_path, [:MDB_env, :pointer], :int
    attach_function :mdb_env_get_fd, [:MDB_env, :pointer], :int
    attach_function :mdb_env_set_mapsize, [:MDB_env, :size_t], :int
    attach_function :mdb_env_set_maxreaders, [:MDB_env, :uint], :int
    attach_function :mdb_env_get_maxreaders, [:MDB_env, :pointer], :int
    attach_function :mdb_env_set_maxdbs, [:MDB_env, :MDB_dbi], :int
    attach_function :mdb_env_get_maxkeysize, [:MDB_env], :int
    #attach_function :mdb_env_set_userctx, [:MDB_env, :pointer], :int # @since 0.9.11
    #attach_function :mdb_env_get_userctx, [:MDB_env], :pointer # @since 0.9.11
    #attach_function :mdb_env_set_assert, [:MDB_env, :MDB_assert_func], :int # not needed

    # Functions on MDB_txn* handles
    attach_function :mdb_txn_begin, [:MDB_env, :MDB_txn, :uint, :pointer], :int
    #attach_function :mdb_txn_env, [:MDB_txn], :MDB_env # not needed
    attach_function :mdb_txn_commit, [:MDB_txn], :int
    attach_function :mdb_txn_abort, [:MDB_txn], :void
    attach_function :mdb_txn_reset, [:MDB_txn], :void
    attach_function :mdb_txn_renew, [:MDB_txn], :int

    # Functions on MDB_dbi handles
    attach_function :mdb_dbi_open, [:MDB_txn, :string, :uint, :pointer], :int
    attach_function :mdb_stat, [:MDB_txn, :MDB_dbi, :MDB_stat], :int
    attach_function :mdb_dbi_flags, [:MDB_txn, :MDB_dbi, :pointer], :int
    attach_function :mdb_dbi_close, [:MDB_txn, :MDB_dbi], :void
    attach_function :mdb_drop, [:MDB_txn, :MDB_dbi, :int], :int
    attach_function :mdb_set_compare, [:MDB_txn, :MDB_dbi, :MDB_cmp_func], :int
    attach_function :mdb_set_dupsort, [:MDB_txn, :MDB_dbi, :MDB_cmp_func], :int
    attach_function :mdb_set_relfunc, [:MDB_txn, :MDB_dbi, :MDB_rel_func], :int
    attach_function :mdb_set_relctx, [:MDB_txn, :MDB_dbi, :pointer], :int
    attach_function :mdb_get, [:MDB_txn, :MDB_dbi, :MDB_val, :MDB_val], :int
    attach_function :mdb_put, [:MDB_txn, :MDB_dbi, :MDB_val, :MDB_val, :uint], :int
    attach_function :mdb_del, [:MDB_txn, :MDB_dbi, :MDB_val, :MDB_val], :int

    # Functions on MDB_cursor* handles
    attach_function :mdb_cursor_open, [:MDB_txn, :MDB_dbi, :pointer], :int
    attach_function :mdb_cursor_close, [:MDB_cursor], :void
    attach_function :mdb_cursor_renew, [:MDB_txn, :MDB_cursor], :int
    #attach_function :mdb_cursor_txn, [:MDB_cursor], :MDB_txn # not needed
    #attach_function :mdb_cursor_dbi, [:MDB_cursor], :MDB_dbi # not needed
    attach_function :mdb_cursor_get, [:MDB_cursor, :MDB_val, :MDB_val, :MDB_cursor_op], :int
    attach_function :mdb_cursor_put, [:MDB_cursor, :MDB_val, :MDB_val, :uint], :int
    attach_function :mdb_cursor_del, [:MDB_cursor, :uint], :int
    attach_function :mdb_cursor_count, [:MDB_cursor, :pointer], :int

    # Functions of miscellaneous kind
    #attach_function :mdb_cmp, [:MDB_txn, :MDB_dbi, :MDB_val, :MDB_val], :int # not needed
    #attach_function :mdb_dcmp, [:MDB_txn, :MDB_dbi, :MDB_val, :MDB_val], :int # not needed
    attach_function :mdb_reader_list, [:MDB_env, :MDB_msg_func, :pointer], :int
    attach_function :mdb_reader_check, [:MDB_env, :pointer], :int

    refine FFI::AutoPointer do
      def valid?() @releaser.valid? end
    end

    refine FFI::AutoPointer::Releaser do
      def valid?() !(@ptr.nil?) end
    end
  end # Library
end # LMDB
