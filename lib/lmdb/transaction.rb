module LMDB
  ##
  # LMDB transaction.
  #
  # @see http://symas.com/mdb/doc/group__internal.html#structMDB__txn
  class Transaction < FFI::AutoPointer
    include Library
    using Library

    ##
    # Constructor.
    #
    # @param [Environment] env the environment
    # @param [Hash] options
    # @raise [Error] if the operation failed
    def initialize(env, options = {})
      flags = 0 # TODO
      ptr = nil
      FFI::MemoryPointer.new(:pointer) do |ptr_to_ptr|
        if (rc = mdb_txn_begin(env, nil, flags, ptr_to_ptr)).nonzero?
          raise Error.new(rc, :mdb_txn_begin)
        end
        ptr = ptr_to_ptr.get_pointer(0)
      end
      super(ptr, self.class.method(:release))
      @env = env
    end

    ##
    # Releases foreign memory associated with this structure.
    #
    # @param  [FFI::Pointer] ptr
    # @return [void]
    def self.release(ptr)
      #Library.mdb_txn_abort(ptr) # FIXME: environment must stick around until here
    end

    ##
    # The environment this transaction belongs to.
    #
    # @return [Environment]
    attr_reader :env

    ##
    # Checks whether this transaction is active.
    #
    # @return [Boolean] `true` or `false`
    def valid?
      super
    end

    ##
    # Commits this transaction.
    #
    # @return [void]
    # @raise  [Error] if the operation failed
    def commit!
      raise "transaction already closed" unless valid?
      if (rc = mdb_txn_commit(self)).nonzero?
        raise Error.new(rc, :mdb_txn_commit)
      end
      self.free
    end
    alias_method :commit, :commit!

    ##
    # Aborts this transaction.
    #
    # @return [void]
    def abort!
      raise "transaction already closed" unless valid?
      mdb_txn_abort(self)
      self.free
    end
    alias_method :abort, :abort!
  end # Transaction
end # LMDB
