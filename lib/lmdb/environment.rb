module LMDB
  ##
  # LMDB environment.
  class Environment < FFI::AutoPointer
    include Library

    ##
    # Constructor.
    #
    # @raise [Error] if the operation failed
    def initialize
      ptr = nil
      FFI::MemoryPointer.new(:pointer) do |ptr_to_ptr|
        if (rc = mdb_env_create(ptr_to_ptr)).nonzero?
          raise Error.new(rc, :mdb_env_create)
        end
        ptr = ptr_to_ptr.get_pointer(0)
      end
      super(ptr, self.class.method(:release))
    end

    ##
    # Releases foreign memory associated with this structure.
    #
    # @param  [FFI::Pointer] ptr
    # @return [void]
    def self.release(ptr)
      Library.mdb_env_close(ptr)
    end

    ##
    # Opens this environment.
    #
    # @param  [#to_s] path
    # @param  [#to_i] mode
    # @param  [Hash] options
    # @return [self]
    # @raise  [Error] if the operation failed
    def open(path, mode = 0644, options = {})
      flags = 0 # TODO
      if (rc = mdb_env_open(self, path.to_s, flags, mode.to_i)).nonzero?
        raise Error.new(rc, :mdb_env_open)
      end
      self
    end

    ##
    # Closes this environment.
    #
    # @return [void]
    def close
      self.free
    end

    ##
    # Returns the path to this environment.
    #
    # @return [String]
    attr_reader :path
    def path
      result = nil
      FFI::MemoryPointer.new(:pointer) do |ptr_to_str|
        if (rc = mdb_env_get_path(self, ptr_to_str)).nonzero?
          raise Error.new(rc, :mdb_env_get_path)
        end
        result = ptr_to_str.get_pointer(0).read_string
      end
      result
    end

    ##
    # Sets the size of the memory map to use for this environment.
    #
    # @param  [Integer] size the size in bytes
    # @raise  [Error] if the operation failed
    attr_writer :map_size
    def map_size=(size)
      if (rc = mdb_env_set_mapsize(self, size)).nonzero?
        raise Error.new(rc, :mdb_env_set_mapsize)
      end
    end

    ##
    # Executes a transaction in this environment.
    #
    # @yield [txn]
    # @yieldparam [Transaction] txn
    # @yieldreturn [void] ignored
    # @return [void]
    def transaction(*args, &block)
      raise LocalJumpError, "no block given" unless block_given?
      txn = Transaction.new(self, *args)
      begin
        block.call(txn)
        txn.commit! if txn.valid?
      rescue => error
        txn.abort! if txn.valid?
        raise error
      end
    end
  end # Environment
end # LMDB
