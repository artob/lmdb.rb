module LMDB
  ##
  # LMDB database handle.
  #
  # @see http://symas.com/mdb/doc/group__mdb.html#gadbe68a06c448dfb62da16443d251a78b
  class Database
    ##
    # Initializes a new database object.
    #
    # @param [Environment] env the environment
    # @param [Integer] dbi the database handle
    # @raise [Error] if the operation failed
    def initialize(env, dbi)
      @env, @dbi = env, dbi
    end

    ##
    # The environment this database belongs to.
    #
    # @return [Environment]
    attr_reader :env
  end # Database
end # LMDB
