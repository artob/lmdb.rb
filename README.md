LMDB.rb: Ruby FFI Bindings for LMDB
===================================

This is a comprehensive Ruby FFI binding for the
[LMDB](http://symas.com/mdb/) embedded B+ tree database library.
